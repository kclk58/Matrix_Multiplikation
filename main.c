#include "stdafx.h"
#include <stdio.h>

int main() {
	int m, n, p, q, c, d, k, sum = 0;
	int erst[10][10], zweit[10][10], mult[10][10];
	printf("Reihen und Spalten der ersten Matrix\n");
	scanf_s("%d%d", &m, &n);
	printf("Elemente der Matrix eingeben\n");
	for (c = 0; c < m; c++)
		for (d = 0; d < m; d++)
			scanf_s("%d", &erst[c][d]);

	printf("Reihe und Spalten der zweiten Matrix\n");
	scanf_s("%d%d", &p, &q);
	if (n != p)
		printf("Matrizen können nicht multipliziert werden.\n");
	else {
		printf("Elemente der Matrix eingeben\n");
		for (c = 0; c < p; c++)
			for (d = 0; d < q; d++)
				scanf_s("%d", &zweit[c][d]);

		for (c = 0; c < m; c++) {
			for (d = 0; d < q; d++) {
				for (k = 0; k < p; k++) {
					sum = sum + erst[c][k] * zweit[k][d];
				}
				mult[c][d] = sum;
				sum = 0;
			}
		}
		printf("Produkt der eingesetzten Matrizen:-\n");

		for (c = 0; c < m; c++) {
			for (d = 0; d < q; d++)
				printf("%d\t", mult[c][d]);

			printf("\n");
		}
	}
	getchar();
	return 0;
}