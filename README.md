# README

## Einführung

Dieses Projekt dient zur Multiplikation zweier Matrizen beliebiger Dimension.

## Wie funktioniert das Programm?

Zuerst wird die Dimension der ersten Matrix angegeben. Daraufhin wird nach
dem Inhalt gefragt, der frei wählbar ist. Anschließend wiederholt sich der
Ablauf für die zweite Matrix. Abschließend wird das Produkt der beiden 
Matrizen vom Programm ausgerechnet und ausgegeben.